---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Crear fork del repositorio principal de actividades
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Crear _fork_ del repositorio principal de actividades

## Acceder al repositorio principal

- Abrir la URL del [repositorio de tareas para la materia][repositorio-tareas]

| ![](img/001-Main_repo.png)
|:--------------------------:|
|

- [Iniciar sesión en GitLab][gitlab-login]

| ![](img/002-Sign_in.png)
|:------------------------:|
| Utiliza tu correo electrónico `@ciencias` para crear una cuenta en GitLab
| <span class="red">No utilices el botón de iniciar sesión con Google, GitHub ni redes sociales</span>

--------------------------------------------------------------------------------

## Haz _fork_ del repositorio principal

- Dar clic en el botón **fork** para crear una copia del repositorio `tareas-redes` en tu cuenta de usuario

| ![](img/003-Fork_repo.png)
|:--------------------------:|
|

- Selecciona tu usuario, marca el repositorio como **público** y espera a que el _fork_ se complete

| ![](img/004-Fork_repo-settings.png)
|:-----------------------------------:|
|

--------------------------------------------------------------------------------

## Da permisos en tu repositorio a los demás miembros de tu equipo

!!! warning
    Estos pasos son necesarios para las entregas en equipo

- Accede a la sección **members** de tu repositorio

| ![](img/006-Fork_members-menu.png)
|:----------------------------------:|
| Probablemente sea necesario que des clic en el botón `<<` en la esquina inferior izquierda de la página para expandir el menú lateral (_sidebar_)

- Busca el nombre de usuario y da permisos de `maintainer` a los demás integrantes de tu equipo

| ![](img/007-Fork_grant-permissions.png)
|:---------------------------------------:|
|

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando ya tengas tu _fork_ y hayas dado los permisos necesarios

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../instalar-dependencias
[arriba]: ../../workflow
[siguiente]: ../clonar-fork

[gitlab-login]: https://gitlab.com/users/sign_in
[repositorio-personal]: https://gitlab.com/USUARIO/tareas-redes
[repositorio-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-1/tareas-redes.git
